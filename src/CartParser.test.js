import CartParser from './CartParser';
import cartJSON from '../samples/cart.json';

let parser;
let namesHeaders;
let dataTable;
let card;
const path = './samples/cart.csv'

beforeEach(() => {
	parser = new CartParser();
	namesHeaders = parser.schema.columns.map(({ name }) => name).join(',');
	dataTable = 'Mollis consequat,9.00,2'
	card = `
		${namesHeaders}
		${dataTable}
		${dataTable}`;
});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.
	it('Should be return info error  "Expected header to be named "Price" but received badName."', () => {
		const namesHeadersError = parser.schema.columns.map(({ name }, i) => (i !== 1 ? name : 'badName')).join(',');
		const expected = {
			type: 'header',
			row: 0,
			column: 1,
			message: 'Expected header to be named "Price" but received badName.'
		};

		expect(parser.validate(namesHeadersError)[0]).toEqual(expected)
	});

	it('Should be return info error  "Expected row to have 3 cells but received 2.', () => {
		const dataTableError = 'Mollis consequat,9.00';
		const cardError = `
		  ${namesHeaders}
      ${dataTableError}`;

		const expected = {
			type: 'row',
			row: 1,
			column: -1,
			message: 'Expected row to have 3 cells but received 2.'
		}

		expect(parser.validate(cardError)[0]).toEqual(expected);
	});

	it('Should be return info error  "Expected cell to be a nonempty string but received "".".', () => {
		const dataTableError = ',9.00,2';
		const cardError = `
		  ${namesHeaders}
      ${dataTableError}`;

		const expected = {
			type: 'cell',
			row: 1,
			column: 0,
			message: 'Expected cell to be a nonempty string but received "".'
		}

		expect(parser.validate(cardError)[0]).toEqual(expected);
	});

	it('Should be return info error  "Expected cell to be a positive number but received "String".', () => {
		const dataTableError = 'Mollis consequat,String,2'
		const cardError = `
		  ${namesHeaders}
      ${dataTableError}`;

		const expected = {
			type: 'cell',
			row: 1,
			"column": 1,
			"message": "Expected cell to be a positive number but received \"String\".",
		}

		expect(parser.validate(cardError)[0]).toEqual(expected)
	});

	it('Should be return info error  "Expected cell to be a positive number but received "-9.00".".', () => {
		const dataTableError = 'Mollis consequat,-9.00,2'
		const cardError = `
		  ${namesHeaders}
      ${dataTableError}`;

		const expected = {
			type: 'cell',
			row: 1,
			column: 1,
			message: 'Expected cell to be a positive number but received "-9.00".'
		}

		expect(parser.validate(cardError)[0]).toEqual(expected)
	});

	it('Should be return info error  "Expected cell to be a positive number but received "NaN".".', () => {
		const dataTableError = 'Mollis consequat, NaN,2'
		const cardError = `
		  ${namesHeaders}
      ${dataTableError}`;

		const expected = {
			type: 'cell',
			row: 1,
			column: 1,
			message: 'Expected cell to be a positive number but received "NaN".'
		}

		expect(parser.validate(cardError)[0]).toEqual(expected)
	});

	it('Should be return error  "Validation failed!"', () => {
		const namesHeadersError = parser.schema.columns.map(({ name }, i) => (i !== 1 ? name : 'badName')).join(',');
		parser.readFile = jest.fn(() => namesHeadersError);

		const expectedErrorMessage = 'Validation failed!'
		expect(() => parser.parse(path)).toThrow(expectedErrorMessage);
	});

	it('Should be two items in the order', () => {
		parser.readFile = jest.fn(() => card);
		const expectedNumberString = 2;

		expect(parser.parse(path).items.length).toBe(expectedNumberString);
	});

	it('should be cost of the order equal 25', () => {
		const dataTable1 = 'Condimentum aliquet,8,2'
		// cost = 9 × 2 + 8 × 2 = 34
		const expectedSum = 34; // total
		const cardForCostCheck = `
		${namesHeaders}
		${dataTable}
		${dataTable1}`;

		parser.readFile = jest.fn(() => cardForCostCheck);

		expect(parser.parse(path).total).toBe(expectedSum);
	});

	it('should be the standard string CSV converted to the expected string JSON', () => {
		const expectedItemsWithId111 = {
			"id": 111,
			"name": "Mollis consequat",
			"price": 9,
			"quantity": 2
		};
		const cardForConvertedCheck = `
		  ${namesHeaders}
      ${dataTable}`;

		parser.readFile = jest.fn(() => card);
		const getIdEqual111 = ({ items }) => items.map(item => ({ ...item, id: 111 }))[0];

		expect(getIdEqual111(parser.parse(path)))
			.toEqual(expectedItemsWithId111);
	});
});

describe('CartParser - integration test', () => {
	// Add your integration test here.
	it('should be the standard table CSV converted to the expected table JSON', () => {
		const getIdEqual111 = ({ items }) => items.map(item => ({ ...item, id: 111 }));
		const expectedSum = 348.32;
		const received = parser.parse(path);

		const expectedItemsWithId111 = getIdEqual111(cartJSON);
		const receivedItemsWithId111 = getIdEqual111(received);

		expect({ ...received, items: receivedItemsWithId111 })
			.toEqual({ total: expectedSum, items: expectedItemsWithId111 });
	});
});
